/**
 * Sends an e-mail using the e-mail API.
 */
export declare function sendEmail(opts: EmailOpts): Promise<{}>;
interface EmailOpts {
    template: string;
    locale?: 'en' | 'sv';
    subject: string;
    recipients: string | string[];
    args?: {
        [key: string]: string;
    };
}
export {};
